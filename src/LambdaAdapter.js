const Configurapi = require('configurapi');
const URL = require('url');

module.exports = {
    toResponse: function(response)
    {
        return {
            statusCode: response.statusCode,
            headers: response.headers,
            body: JSON.stringify(response.body)
        };
    },

    toRequest: function(awsRequest)
    {
        let request = new Configurapi.Request();

        request.name = awsRequest.name || process.env.event_name || '';
        request.method = '';
        request.headers = {};

        request.payload = awsRequest.Records;
        request.query = {};

        request.path = '';
        request.pathAndQuery = '';

        try
        {
            if(request.payload)
            {
                request.payload = JSON.parse(request.payload);
            }
        }
        catch(e){}

        return request;
    }
};
